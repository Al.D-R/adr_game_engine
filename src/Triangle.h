#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Vector3D.h"

namespace GE {

  class Triangle {
  private:
    Vector3D u, v, w;
  public:
    Triangle(Vector3D vec1, Vector3D vec2,  Vector3D vec3);
    Triangle(Triangle const& tri);
    ~Triangle();

    void operator=(Triangle const& tri);
    friend bool operator==(Triangle const& a, Triangle const& b);
    friend bool operator!=(Triangle const& a, Triangle const& b);

    Vector3D getU();
    Vector3D getV();
    Vector3D getW();

    void setU(Vector3D vec);
    void setV(Vector3D vec);
    void setW(Vector3D vec);
  };

}
#endif // TRIANGLE_H
