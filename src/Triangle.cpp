#include "Triangle.h"
using namespace std;

namespace GE {

  Triangle::Triangle(Vector3D vec1, Vector3D vec2,  Vector3D vec3): u(vec1), v(vec2), w(vec3) {}
  Triangle::Triangle(Triangle const& tri): u(tri.u), v(tri.v), w(tri.w) {}
  Triangle::~Triangle() {}

  void Triangle::operator=(Triangle const& tri) {
    u = tri.u;
    v = tri.v;
    w = tri.w;
  }
  bool operator==(Triangle const& a, Triangle const& b) {
    return (a.u == b.u && a.v == b.v && a.w == b.w);
  }
  bool operator!=(Triangle const& a, Triangle const& b) {
    return !(a == b);
  }

  Vector3D Triangle::getU() { return u; }
  Vector3D Triangle::getV() { return v; }
  Vector3D Triangle::getW() { return w; }

  void Triangle::setU(Vector3D vec) { u = vec; }
  void Triangle::setV(Vector3D vec) { v = vec; }
  void Triangle::setW(Vector3D vec) { w = vec; }

}
