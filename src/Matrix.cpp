#include "Matrix.h"
using namespace std;

namespace GE {

  Matrix::Matrix(int n_rows, int n_cols): rows(n_rows), cols(n_cols) {
    constructValues(n_rows, n_cols);
  }

  Matrix::Matrix(int n_rows, int n_cols, float a): rows(n_rows), cols(n_cols) {
    constructValues(n_rows, n_cols);
    for (int i=0; i < rows; i++) {
      for (int j=0; j < cols; j++)
        values[i][j] = a;
    }
  }

  Matrix::Matrix(Matrix const& m): cols(m.cols), rows(m.rows) {
    constructValues(m.rows, m.cols);
    for (int i=0; i < rows; i++) {
      for (int j=0; j < cols; j++)
        values[i][j] = m.values[i][j];
    }
  }

  Matrix::~Matrix() {
    for (int i=0; i < cols; i++)
      delete[] values[i];
    delete[] values;
  }

  float Matrix::operator()(int a, int b) {
    return values[a][b];
  }
  void Matrix::operator()(int a, int b, float c) {
    values[a][b] = c;
  }
  void Matrix::operator=(Matrix m) {
    if (m.rows == rows && m.cols == cols) {
      for (int i=0; i < rows; i++) {
        for (int j=0; j < cols; j++) {
          values[i][j] = m.values[i][j];
        }
      }
    }
  }
  bool operator==(Matrix a, Matrix b) {
    if (a.rows == b.rows && a.cols == b.cols) {
      for (int i=0; i < a.rows; i++) {
        for (int j=0; j < a.cols; j++) {
          if (a.values[i][j] == b.values[i][j])
            return true;
          else
            return false;
        }
      }
    }
    else
      return false;
  }
  bool operator!=(Matrix a, Matrix b) {
    return !(a == b);
  }

  void Matrix::operator*=(float a) {
    for (int i=0; i < rows; i++) {
      for (int j=0; j < cols; j++) {
        values[i][j] *= a;
      }
    }
  }

  Matrix operator*(Matrix a, float b) {
    Matrix r(a);
    r *= b;
    return r;
  }

  Matrix operator*(Matrix a, Matrix b) {
    if (a.cols == b.rows) {
      Matrix m(a.rows, b.cols, 0);
      for (int row=0; row < a.rows; row++) {
        for (int col=0; col < b.cols; col++) {
          for (int i=0; i < a.cols; i++) {
            //cout << a.values[row][i] << " * " << b.values[i][col] << " = " << (a.values[row][i] * b.values[i][col]) << " + " << endl;
            m.values[row][col] += (a.values[row][i] * b.values[i][col]);
          }
          //cout << "val : " << m.values[row][col] << endl;
        }
      }
      return m;
    }
    else
      return a;
  }

  ostream& operator<<(ostream& stream, Matrix m) {
    for (int i=0; i < m.rows; i++) {
      for (int j=0; j < m.cols; j++) {
        stream << m(i, j) << " ";
        if (j == (m.cols - 1) && i != (m.rows -1))
          stream << endl;
      }
    }
    return stream;
  }

  //static
  Matrix Matrix::identity(int n) {
    Matrix m(n, n);
    for (int i=0; i < n; i++) {
      for (int j=0; j < n; j++) {
        if (i == j)
          m.values[i][j] = 1;
        else
          m.values[i][j] = 0;
      }
    }
    return m;
  }

  void Matrix::constructValues(int rows, int cols) {
    values = new float*[rows];
    for (int i=0; i < rows; i++)
      values[i] = new float[cols];
  }

}
