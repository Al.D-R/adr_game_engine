#include "Mesh.h"
using namespace std;

namespace GE {

  Mesh::Mesh() {}
  Mesh::Mesh(Mesh const& mesh) {
    for (int i=0; i < mesh.triangles.size(); i++)
      triangles.push_back(mesh.triangles[i]);
  }
  Mesh::~Mesh() {}

  void Mesh::operator=(Mesh const& mesh) {
    for (int i=triangles.size(); i >= 0; i--)
      triangles.pop_back();
    for (int i=0; i < mesh.triangles.size(); i++)
      triangles.push_back(mesh.triangles[i]);
  }
  bool operator==(Mesh const& a, Mesh const& b) {
    if (a.triangles.size() != b.triangles.size())
      return false;
    else {
      for (int i=0; i < a.triangles.size(); i++) {
        if (a.triangles[i] != b.triangles[i])
          return false;
      }
    }
    return true;
  }
  bool operator!=(Mesh const& a, Mesh const& b) {
    return !(a == b);
  }

  //static
  Mesh Mesh::drawCube(Vector3D origin, float lengh) {
    Mesh cube;
    //South
    cube.addTriangle(Triangle(Vector3D(0,0,0), Vector3D(0,1,0), Vector3D(1,1,0)));
    cube.addTriangle(Triangle(Vector3D(0,0,0), Vector3D(1,8,0), Vector3D(1,0,0)));
    //East
    cube.addTriangle(Triangle(Vector3D(1,0,0), Vector3D(1,1,0), Vector3D(1,1,1)));
    cube.addTriangle(Triangle(Vector3D(1,0,0), Vector3D(1,1,1), Vector3D(1,0,1)));
    //North
    cube.addTriangle(Triangle(Vector3D(1,0,1), Vector3D(1,1,1), Vector3D(0,1,1)));
    cube.addTriangle(Triangle(Vector3D(1,0,1), Vector3D(0,1,1), Vector3D(0,0,1)));
    //West
    cube.addTriangle(Triangle(Vector3D(0,0,1), Vector3D(0,1,1), Vector3D(0,1,0)));
    cube.addTriangle(Triangle(Vector3D(0,0,1), Vector3D(0,1,0), Vector3D(0,0,0)));
    //Top
    cube.addTriangle(Triangle(Vector3D(0,1,0), Vector3D(0,1,1), Vector3D(1,1,1)));
    cube.addTriangle(Triangle(Vector3D(0,1,0), Vector3D(1,1,1), Vector3D(1,1,0)));
    //Bottom
    cube.addTriangle(Triangle(Vector3D(1,0,0), Vector3D(1,0,1), Vector3D(0,0,1)));
    cube.addTriangle(Triangle(Vector3D(1,0,0), Vector3D(0,0,1), Vector3D(0,0,0)));
    return cube;
  }

  Triangle Mesh::getTriangle(int index) { return triangles[index]; }
  void Mesh::setTriangle(int index, Triangle tri) {
    triangles[index] = tri;
  }

  void Mesh::addTriangle(Triangle tri) {
    triangles.push_back(tri);
  }
  void Mesh::delLastTriangle() {
    triangles.pop_back();
  }

}
