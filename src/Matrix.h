#ifndef MAfloatRIX_H
#define MAfloatRIX_H

#include <iostream>

namespace GE {

  class Matrix {
  private:
    int cols, rows;
    float** values;
    void constructValues(int rows, int cols);

  public:

    Matrix(int n_rows, int n_cols);
    Matrix(Matrix const& m);
    Matrix(int n_rows, int n_cols, float a);
    ~Matrix();
    float operator()(int a, int b);
    void operator()(int a, int b, float c);
    void operator=(Matrix m);
    friend bool operator==(Matrix a, Matrix b);
    friend bool operator!=(Matrix a, Matrix b);
    void operator*=(float a);
    friend Matrix operator*(Matrix a, float b);
    friend Matrix operator*(Matrix a, Matrix b);
    friend std::ostream& operator<<(std::ostream& stream, Matrix m);

    static Matrix identity(int n);

  };
}
#endif //MAfloatRIX_H
