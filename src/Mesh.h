#ifndef MESH_H
#define MESH_H

#include <vector>
#include "Triangle.h"

namespace GE {

  class Mesh {
  private:
    std::vector<Triangle> triangles;
  public:
    Mesh();
    Mesh(Mesh const& mesh);
    ~Mesh();

    void operator=(Mesh const& mesh);
    friend bool operator==(Mesh const& a, Mesh const& b);
    friend bool operator!=(Mesh const& a, Mesh const& b);

    static Mesh drawCube(Vector3D origin, float lengh);

    Triangle getTriangle(int index);
    void setTriangle(int index, Triangle tri);
    void addTriangle(Triangle tri);
    void delLastTriangle();
  };

}

#endif //MESH_H
