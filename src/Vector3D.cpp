#include "Vector3D.h"

using namespace std;

namespace GE {

  Vector3D::Vector3D(float a, float b, float c) : x(a), y(b), z(c) {}
  Vector3D::Vector3D(Vector3D const& vec) : x(vec.x), y(vec.y), z(vec.z) {}
  Vector3D::~Vector3D() {}

  //Operators
  void Vector3D::operator=(Vector3D const& vec) {
    x = vec.x;
    y = vec.y;
    z = vec.z;
  }
  void Vector3D::operator+=(Vector3D const& vec) {
    x += vec.x;
    y += vec.y;
    z += vec.z;
  }
  Vector3D operator+(Vector3D const& u, Vector3D const& v) {
    Vector3D w(u);
    w += v;
    return w;
  }
  void Vector3D::operator-=(Vector3D const& vec) {
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
  }
  Vector3D operator-(Vector3D const& u, Vector3D const& v) {
    Vector3D w(u);
    w -= v;
    return w;
  }
  void Vector3D::operator*=(float a) {
    x *= a;
    y *= a;
    z *= a;
  }
  Vector3D operator*(Vector3D const& u, float a) {
    Vector3D v(u);
    v *= a;
    return v;
  }
  void Vector3D::operator/=(float a) {
    x /= a;
    y /= a;
    z /= a;
  }
  Vector3D operator/(Vector3D const& u, float a)
  {
    Vector3D w(u);
    w /= a;
    return w;
  }
  bool operator==(Vector3D const& u, Vector3D const& v) {
    return (u.x == v.x && u.y == v.y && u.z == v.z);
  }
  bool operator!=(Vector3D const& u, Vector3D const& v) {
    return !(u == v);
  }

  float Vector3D::norm() const {
    return sqrt(x*x+y*y+z*z);
  }

  //Set & Get
  float Vector3D::getX() const {return x;}
  float Vector3D::getY() const {return y;}
  float Vector3D::getZ() const {return z;}
  void Vector3D::setX(float a) {x = a;}
  void Vector3D::setY(float a) {y = a;}
  void Vector3D::setZ(float a) {z = a;}

}
