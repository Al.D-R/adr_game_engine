#ifndef VECTOR3D_H
#define VECTOR3D_H
#include <cmath>

namespace GE {

  class Vector3D {
  private:
    float x, y, z;
  public:
    Vector3D(float a, float b, float c);
    Vector3D(Vector3D const& vec);
    ~Vector3D();

    //Operators
    void operator=(Vector3D const& vec);
    void operator+=(Vector3D const& vec);
    friend Vector3D operator+(Vector3D const& u, Vector3D const& v);
    void operator-=(Vector3D const& vec);
    friend Vector3D operator-(Vector3D const& u, Vector3D const& v);
    void operator*=(float a);
    friend Vector3D operator*(Vector3D const& u, float a);
    void operator/=(float a);
    friend Vector3D operator/(Vector3D const& u, float a);
    friend bool operator==(Vector3D const& u, Vector3D const& v);
    friend bool operator!=(Vector3D const& u, Vector3D const& v);

    float norm() const;

    //Set & Get
    float getX() const;
    float getY() const;
    float getZ() const;
    void setX(float a);
    void setY(float a);
    void setZ(float a);
  };

}
#endif // VECTOR3D_H
