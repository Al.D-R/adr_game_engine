#include <iostream>
#include <typeinfo>
#include "Mesh.h"
#include "Matrix.h"

using namespace std;
using namespace GE;

int main() {

  Matrix m1(4, 4, 0);
  m1(0, 0, 0);
  m1(0, 1, 1);
  m1(0, 2, 2);
  m1(0, 3, 3);
  m1(1, 0, 4);
  m1(1, 1, 5);
  m1(1, 2, 6);
  m1(1, 3, 7);
  m1(2, 0, 8);
  m1(2, 1, 9);
  m1(2, 2, 10);
  m1(2, 3, 11);
  m1(3, 0, 12);
  m1(3, 1, 13);
  m1(3, 2, 14);
  m1(3, 3, 15);
  Matrix m2(4, 4, 0);
  m2(0, 0, 30);
  m2(0, 1, 31);
  m2(0, 2, 32);
  m2(0, 3, 33);
  m2(1, 0, 34);
  m2(1, 1, 35);
  m2(1, 2, 36);
  m2(1, 3, 37);
  m2(2, 0, 38);
  m2(2, 1, 39);
  m2(2, 2, 40);
  m2(2, 3, 41);
  m2(3, 0, 42);
  m2(3, 1, 43);
  m2(3, 2, 44);
  m2(3, 3, 45);

  Matrix r(m1 * m2);

  cout << m1 << endl << " x " << endl << m2 << endl << " = " << endl << r << endl;

    return 0;
}
